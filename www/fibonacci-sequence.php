<?php

// TODO: Fill in the empty comments to describe what each line of code is doing
// do not change any of the code
function make_Fibonacci_Sequence($length){
  $fibonacci = [];               //creates an empty array called fibonacci
  for($i=0; $i<$length; $i++){   //for each +1 increment of the number passed, find_next_in_sequence is called
    /*
    */
    $fibonacci[] = find_Next_In_Sequence($fibonacci);
  }
  return $fibonacci;             //returns the array
}

// TODO: Edit this function to make the page work as intended
function find_Next_In_Sequence($list){
  $number1 = end($list);                           // 1. find the last number that was added to the list
  if (count($list)<=1){$answer=0;}                 // 2. if there were no previous numbers, the answer is 0
  $number2 = prev($list);                          // 3. find the second to last number added to the list
  if (count($list)==1){$answer=1;}else{            // 4. if there is only one number in the list, the answer is 1
  if (count($list)>=2){$answer=$number1+$number2;} // 5. if there are two or more numbers in the list,
                                                   // the answer is the last number added to the second to last number
  }
      return $answer;                              // 6. return the answer

}
// set up the global variable `$fibonacci` with 
// the first 10 numbers in the fibonacci sequence
$fibonacci = make_Fibonacci_Sequence(10);
// If done correctly, the sequence should return
//   [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]


?><!DOCTYPE html>
<html>
  <body>
    <h1>Fibonacci!</h1>
    <?php foreach($fibonacci as $number){ ?>
      <li><?= $number ?></li>
    <?php } ?>
  </body>
</html>
